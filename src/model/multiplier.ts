export class Multiplier {
  private static _instance: Multiplier

  private global: number = 1
  private click: number = 1

  private constructor() {
  }

  public static get Instance() {
    return this._instance || (this._instance = new this())
  }

  get getGlobal(): number {
    return this.global
  }

  get getClick(): number{ 
    return this.click * this.global
  }

  set setGlobal(global: number) {
    if (global <= 0) {
      new RangeError("Multiplier must be more than zero!")
    }

    this.global = global
  }

  set setClick(click: number) {
    if (click <= 0) {
      new RangeError("Multiplier must be more than zero!")
    }

    this.click = click
  }
}

export const multiplier = Multiplier.Instance
