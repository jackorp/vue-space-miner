export default class Material {
  public name: string
  public icon: string
  public count: number

  constructor(name: string, icon: string, count: number) {
    this.name = name
    this.icon = icon
    this.count = count
  }
}
