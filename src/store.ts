import Vue from 'vue'
import Vuex from 'vuex'
import Material from '@/model/material'
import { MaterialsData } from '@/model/materialsData'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    materials: MaterialsData
  },

  mutations: {
    saveMaterials(state) {
      for (var material in state.materials) {
	return // TODO: NYI
      }
    }
  },
  actions: {
    saveMaterials(context) {
      context.commit('saveMaterials')
    }
  }
})
